backend port 5000

routen:
    /reserve
        get (get): {
            id
            startdate
            enddate
            symbol
            roomid
        }
        post (add): {
            startdate
            enddate
            symbol
            roomid
            usedCapacity
        }
        delete (delete): {
            id
        }
        put (edit): {
            id
            startdate
            enddate
            symbol
            roomid
            usedCapacity
        }
    
    /rooms
        get (get): {
            id
            name
        }
        post (add): {
            name
            capacity
        }
        delete (delete): {
            id
        }
        put (edit): {
            id
            name
            capacity
        }
