﻿using RaumPlanung.MVVM;
using RaumPlanung.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaumPlanung.ViewModels.ViewModels
{
    public class RoomViewModel : ObservableObject
    {
        private RoomModel _SelectedRoom;
        public RoomModel SelectedRoom
        {
            get
            { 
                if(_SelectedRoom == null && _Rooms.Count > 0)
                {
                    return _Rooms[0];
                }
                else
                {
                    return _SelectedRoom;
                }
            }
            set => Set("SelectedRoom", ref _SelectedRoom, value);
        }

        private List<RoomModel> _Rooms = new List<RoomModel>() {
            new RoomModel("303", 20),
            new RoomModel("304", 30),
        };
        public List<RoomModel> Rooms
        {
            get => _Rooms;
            set => Set("Rooms", ref _Rooms, value);
        }
    }
}
