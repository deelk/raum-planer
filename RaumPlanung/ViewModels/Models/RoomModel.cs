﻿using RaumPlanung.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaumPlanung.ViewModels.Models
{
    public class RoomModel : ObservableObject
    {
        private string _Name;
        public string Name
        {
            get => _Name;
            set => Set("Name", ref _Name, value);
        }

        private int _Capacity;
        public int Capacity
        {
            get => _Capacity;
            set => Set("Capacity", ref _Capacity, value);
        }

        public RoomModel(string name, int capacity)
        {
            this.Name = name;
            this.Capacity = capacity;
        }
    }
}
