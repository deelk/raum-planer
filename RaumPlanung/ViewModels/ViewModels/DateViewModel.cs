﻿using RaumPlanung.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RaumPlanung.ViewModels.ViewModels
{
    public class DateViewModel : ObservableObject
    {
        DateTime _SelectedDate = DateTime.Now;
        public DateTime SelectedDate { 
            get => _SelectedDate; 
            set => Set("SelectedDate", ref _SelectedDate, value); 
        }
        
        public DateViewModel()
        {
            
        }


    }
}
