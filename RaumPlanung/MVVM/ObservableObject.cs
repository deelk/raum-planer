﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaumPlanung.MVVM
{
    public class ObservableObject : INotifyPropertyChanged
    {
        private Dictionary<string, List<string>> RegisteredValues = new Dictionary<string, List<string>>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected void Set<T>(string propertyName, ref T property, T value)
        {
            property = value;
            OnPropertyChanged(propertyName);
        } 

        protected void RegisterProperty(string masterProperty, string slaveProperty)
        {
            if(RegisteredValues.TryGetValue(masterProperty, out var list))
            {
                list.Add(slaveProperty);
            }
            else
            {
                RegisteredValues.Add(masterProperty, new List<string>() { slaveProperty });
            }
        }
        protected void RegisterProperty(string masterProperty, IEnumerable<string> slaveProperties)
        {
            if (RegisteredValues.TryGetValue(masterProperty, out var list))
            {
                list.AddRange(slaveProperties);
            }
            else
            {
                RegisteredValues.Add(masterProperty, slaveProperties.ToList());
            }
        }
    }
}
