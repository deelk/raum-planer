﻿using RaumPlanung.ViewModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RaumPlanung
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DateViewModel DateViewModel { get; set; } = new DateViewModel();
        public RoomViewModel RoomViewModel { get; set; } = new RoomViewModel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
